const DEBUG = 5;
const INFO = 4;
const WARN = 3;
const ERROR = 2;
const FATAL = 1;
const OFF = 0;

function Logger() {
  let _level;
  let instance;

  function log(level, message, name) {
    if (level > _level) return;
    const date = new Date().toISOString();
    let logLevel;
    switch (level) {
      case DEBUG:
        logLevel = "DEBUG";
        break;
      case INFO:
        logLevel = "INFO";
        break;
      case WARN:
        logLevel = "WARN";
        break;
      case ERROR:
        logLevel = "ERROR";
        break;
      case FATAL:
        logLevel = "FATAL";
        break;
    }
    console.log(`(${date}) [${logLevel}] [${name}]: ${message}`);
  }

  function setLogLevel(level) {
    _level = level;
  }

  function createNamespace(name) {
    let namespaceInstance = {
      debug: (message) => log(DEBUG, message, name),
      info: (message) => log(INFO, message, name),
      warn: (message) => log(WARN, message, name),
      error: (message) => log(ERROR, message, name),
      fatal: (message) => log(FATAL, message, name),
    };

    return namespaceInstance;
  }

  instance = {
    setLogLevel,
    createNamespace,
  };

  return instance;
}

const logger = new Logger();
logger.DEBUG = DEBUG;
logger.INFO = INFO;
logger.WARN = WARN;
logger.ERROR = ERROR;
logger.FATAL = FATAL;
logger.OFF = OFF;

module.exports = logger;
