const File = require("./file");
const JobQueue = require("../utils/job-queue");
const Logger = require("../utils/logger");
const EventBus = require("../core/event-bus");
const {
  DIRECTORY_SCAN_STARTED,
  DIRECTORY_SCAN_FINISHED,
} = require("../core/events");

const logger = Logger.createNamespace("DirectoryScanner");

const MAX_READING_JOBS = 5;

function DirectoryScanner() {
  let _readingQueue = new JobQueue(MAX_READING_JOBS);
  let _queueReading = _readingQueue.queueJob.bind(_readingQueue);
  let _fileExtensions, _fileSystem;

  function setConfig({ fileSystem, extensions } = {}) {
    _fileSystem = fileSystem;
    extensions = extensions || [];
    _fileExtensions = extensions.map((extension) =>
      _determineFileType(extension)
    );

    logger.debug(
      "Setting config " + JSON.stringify({ fileSystem, extensions })
    );
  }

  function _checkConfig() {
    if (!_fileSystem) {
      const error = new Error("File system not configured!");
      logger.error(error);
      throw error;
    }
    if (
      !_fileExtensions ||
      !(_fileExtensions instanceof Array) ||
      _fileExtensions.length === 0
    ) {
      const error = new Error("No file extensions provided!");
      logger.error(error);
      throw error;
    }
  }

  async function scanDirs(directories, callback) {
    _checkConfig();
    EventBus.dispatch(DIRECTORY_SCAN_STARTED);
    await Promise.all(
      directories.map((directory) => scanDir(directory, callback))
    );
    EventBus.dispatch(DIRECTORY_SCAN_FINISHED);
  }

  async function scanDir(directory, callback) {
    _checkConfig();
    const directories = [directory];
    do {
      await Promise.all(
        directories.map((directory) =>
          _queueReading(async () => {
            logger.debug("Scanning directory " + directory);
            const fileNames = await _fileSystem.readDir(directory);
            logger.debug(`Found ${fileNames.length} files`);
            const filePaths = fileNames.map(
              (fileName) => `${directory}/${fileName}`
            );
            await Promise.all(
              filePaths.map(async (filePath) => {
                const stats = await _fileSystem.stats(filePath);
                if (!stats) return;
                if (stats.isDirectory) {
                  directories.push(filePath);
                  return;
                }
                const fileType = _determineFileType(filePath);
                if (_fileExtensions.indexOf(fileType) === -1) return;
                logger.debug("Found file " + filePath);
                callback(new File(filePath, fileType, null, stats));
              })
            );
            directories.splice(directories.indexOf(directory), 1);
          })
        )
      );
    } while (directories.length > 0);
  }

  function _determineFileType(path) {
    switch (path.split(".").pop().toLowerCase()) {
      case "mp3":
        return File.MP3;
      case "flac":
        return File.FLAC;
      default:
        return null;
    }
  }

  return {
    setConfig,
    scanDirs,
    scanDir,
  };
}

module.exports = DirectoryScanner;
