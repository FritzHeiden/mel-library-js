function Events() {
  this.DIRECTORY_SCAN_STARTED = "onDirectoryScanStarted";
  this.DIRECTORY_SCAN_FINISHED = "onDirectoryScanFinished";
  this.ARTIST_ADDED = "onArtistAdded";
  this.RELEASE_ADDED = "onReleaseAdded";
  this.TRACK_ADDED = "onTrackAdded";
  this.FILE_ADDED = "onFileAdded";
}

let events = new Events();

module.exports = events;
