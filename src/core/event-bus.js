const EventEmitter = require("../utils/event-emitter");

function EventBus() {
  let eventEmitter = new EventEmitter();

  let instance;

  instance = {
    on: eventEmitter.on,
    off: eventEmitter.off,
    dispatch: eventEmitter.dispatch,
  };

  return instance;
}

const eventBus = new EventBus();

module.exports = eventBus;
