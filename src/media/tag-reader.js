const JsMediaTags = require("jsmediatags/dist/jsmediatags.js");

const FileReader = require("./file-reader");
const Logger = require("../utils/logger");
const File = require("../files/file");

const logger = Logger.createNamespace("TagReader");

const MP3_TAGS_TO_READ = [
  "TIT2",
  "TYER",
  "TPOS",
  "TPE2",
  "TRCK",
  "TALB",
  "TPE1",
  "TDAT",
  "TDRC",
];

const FLAC_TAGS_TO_READ = [
  "TITLE",
  "ARTIST",
  "ALBUM",
  "ALBUMARTIST",
  "DATE",
  "DISCNUMBER",
  "TRACKNUMBER",
];

function TagReader() {
  JsMediaTags.Config.addFileReader(FileReader);

  function setConfig({ fileSystem }) {
    FileReader.fileSystem = fileSystem;
    logger.debug("Setting config " + JSON.stringify({ fileSystem }));
  }

  async function readTags(file) {
    let tagsToRead;
    switch (file.getType()) {
      case File.MP3:
        tagsToRead = MP3_TAGS_TO_READ;
        break;
      case File.FLAC:
        tagsToRead = FLAC_TAGS_TO_READ;
        break;
    }
    return new Promise((resolve, reject) => {
      new JsMediaTags.Reader(file).setTagsToRead(tagsToRead).read({
        onSuccess: ({ tags }) => {
          let trackTitle = null;
          if (tags.title) trackTitle = tags.title;
          if (!trackTitle && tags.TIT2) trackTitle = tags.TIT2.data;
          let artistName = null;
          if (tags.artist) artistName = tags.artist;
          if (!artistName && tags.TPE1) artistName = tags.TPE1.data;
          let albumArtistName = null;
          if (tags.albumArtist) albumArtistName = tags.albumArtist;
          if (!albumArtistName && tags.TPE2) albumArtistName = tags.TPE2.data;
          let year = null;
          if (tags.year) year = tags.year;
          if (!year && tags.TDAT) year = tags.TDAT.data;
          if (!year && tags.TYER) year = tags.TYER.data;
          if (!year && tags.TDRC) year = tags.TDRC.data;
          let trackNumber = null;
          if (tags.track) trackNumber = tags.track;
          if (!trackNumber && tags.TRCK) trackNumber = tags.TRCK.data;
          let discNumber = null;
          if (tags.discNumber) discNumber = tags.discNumber;
          if (!discNumber && tags.TPOS) discNumber = tags.TPOS.data;
          let albumTitle = null;
          if (tags.album) albumTitle = tags.album;
          if (!albumTitle && tags.TALB) albumTitle = tags.TALB.data;

          resolve({
            trackTitle,
            artistName,
            albumArtistName,
            albumTitle,
            year,
            trackNumber,
            discNumber,
          });
        },
        onError: (err) => {
          error = new Error(
            `Failed to read tags for file ${file.getPath()}${
              err ? `: ${err.info}` : ""
            }`
          );
          logger.error(error);
          reject(error);
        },
      });
    });
  }

  async function readAlbumCover(file) {
    let tagsToRead;
    switch (file.getType()) {
      case File.MP3:
        tagsToRead = ["APIC"];
        break;
      case File.FLAC:
        tagsToRead = ["PICTURE"];
        break;
    }
    return new Promise((resolve, reject) => {
      new JsMediaTags.Reader(file).setTagsToRead(tagsToRead).read({
        onSuccess: ({ tags }) => {
          let albumCover = null;
          let picture;
          if (tags.APIC) picture = tags.APIC.data;
          if (tags.picture) picture = tags.picture;
          if (picture) {
            let format = picture.format;
            let data = new Uint8Array(picture.data);
            data = data.buffer;
            albumCover = { format, data };
          }

          resolve(albumCover);
        },
        onError: (err) => {
          error = new Error(
            `Failed to read album cover for file ${file.getPath()}${
              err ? `: ${err.info}` : ""
            }`
          );
          logger.error(error);
          reject(error);
        },
      });
    });
  }

  return { setConfig, readTags, readAlbumCover };
}

module.exports = TagReader;
