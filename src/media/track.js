function Track(
  id,
  title = "",
  artists = [],
  release = null,
  number = 0,
  discNumber = 0
) {
  let _title = title;
  let _artists = artists;
  let _release = release;
  let _number = number;
  let _discNumber = discNumber;
  let _id = id;

  function addArtist(newArtist) {
    const index = _artists.findIndex(
      (artist) => artist.getId() === newArtist.getId()
    );
    if (index === -1) {
      _artists.push(newArtist);
    }
  }

  function addArtists(newArtists) {
    newArtists.forEach((artist) => addArtist(artist));
    return instance;
  }

  function complement(track) {
    const complementedTrack = new Track(
      _id || track.getId(),
      _title || track.getTitle(),
      _artists,
      _release || track.getRelease(),
      _number || track.getNumber(),
      _discNumber || track.getDiscNumber()
    );
    complementedTrack.addArtists(track.getArtists());
    return complementedTrack;
  }

  function toString() {
    return (
      `Track {` +
      `title: ${_title}, ` +
      `artists: ${_artists ? _artists.map((artist) => artist.getName()) : []}, ` +
      `release: ${_release.name}, ` +
      `number: ${_number}, ` +
      `discNumber: ${_discNumber}, ` +
      `id: ${_id}}`
    );
  }

  function getTitle() {
    return _title;
  }

  function setTitle(value) {
    _title = value;
    return instance;
  }

  function getArtists() {
    return _artists;
  }

  function setArtists(value) {
    _artists = value;
    return instance;
  }

  function getRelease() {
    return _release;
  }

  function setRelease(value) {
    _release = value;
    return instance;
  }

  function getNumber() {
    return _number;
  }

  function setNumber(value) {
    _number = value;
    return instance;
  }

  function getDiscNumber() {
    return _discNumber;
  }

  function setDiscNumber(value) {
    _discNumber = value;
    return instance;
  }

  function getId() {
    return _id;
  }

  function setId(value) {
    _id = value;
    return instance;
  }

  let instance = {
    addArtist,
    addArtists,
    complement,
    toString,
    getTitle,
    setTitle,
    getArtists,
    setArtists,
    getRelease,
    setRelease,
    getNumber,
    setNumber,
    getDiscNumber,
    setDiscNumber,
    getId,
    setId,
  };

  return instance;
}

module.exports = Track;
