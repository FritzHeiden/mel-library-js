function Release(
  id,
  artist,
  title,
  year,
  tracks = [],
  featureArtists = [],
  releaseCover
) {
  let _id = id;
  let _title = title;
  let _year = year;
  let _tracks = tracks;
  let _artist = artist;
  let _featureArtists = featureArtists;
  let _releaseCover = releaseCover;

  function addTrack(newTrack) {
    newTrack.setRelease(instance);
    if (
      _tracks.findIndex((track) => track.getId() === newTrack.getId()) ===
      -1
    ) {
      _tracks.push(newTrack);
    }
  }

  function addTracks(tracks) {
    tracks.forEach((track) => addTrack(track));
  }

  function addFeatureArtist(newArtist) {
    if (
      _featureArtists.findIndex(
        (artist) => artist.getId() === newArtist.getId()
      ) === -1
    ) {
      _featureArtists.push(newArtist);
      newArtist.addFeatureRelease(instance);
    }
  }

  function addFeatureArtists(artists) {
    artists.forEach((artist) => addFeatureArtist(artist));
  }

  function deleteTrack(trackId) {
    const index = _tracks.findIndex((track) => track.getId() === trackId);
    _tracks.splice(index, 1);
  }

  function complement(release) {
    const complementedRelease = new Release(
      _id || release.getId(),
      _artist || release.getArtist(),
      _title || release.getTitle(),
      _year || release.getYear(),
      _tracks,
      _featureArtists,
      _releaseCover || release.getReleaseCover()
    );
    complementedRelease.addFeatureArtists(release.getFeatureArtists());
    complementedRelease.addTracks(release.getTracks());
    return complementedRelease;
  }

  function toString() {
    return (
      `Release {` +
      `title: ${_title}, ` +
      `year: ${_year}, ` +
      `trackCount: ${_tracks.length}, ` +
      `artist: ${_artist.getName()}, ` +
      `featureArtists: ${_featureArtists.map((artist) =>
        artist.getName()
      )}, ` +
      `id: ${_id}}`
    );
  }

  function deleteReleaseCoverBuffer() {
    _releaseCover = null;
  }

  function getReleaseCover() {
    return _releaseCover;
  }

  function setReleaseCover(cover) {
    _releaseCover = cover;
  }

  function setArtist(artist) {
    _artist = artist;
    return instance;
  }

  function getArtist() {
    return _artist;
  }

  function getTitle() {
    return _title;
  }

  function setTitle(value) {
    _title = value;
    return instance;
  }

  function getYear() {
    return _year;
  }

  function setYear(value) {
    _year = value;
    return instance;
  }

  function getTracks() {
    return _tracks;
  }

  function setTracks(value) {
    _tracks = value;
    return instance;
  }

  function getId() {
    return _id;
  }

  function setId(value) {
    _id = value;
    return instance;
  }

  function getFeatureArtists() {
    return _featureArtists;
  }

  function setFeatureArtists(value) {
    _featureArtists = value;
    return instance;
  }

  let instance = {
    addTrack,
    addTracks,
    addFeatureArtist,
    addFeatureArtists,
    deleteTrack,
    complement,
    toString,
    deleteReleaseCoverBuffer,
    getReleaseCover,
    setReleaseCover,
    setArtist,
    getArtist,
    getTitle,
    setTitle,
    getYear,
    setYear,
    getTracks,
    setTracks,
    getId,
    setId,
    getFeatureArtists,
    setFeatureArtists,
  };

  return instance;
}

module.exports = Release;
