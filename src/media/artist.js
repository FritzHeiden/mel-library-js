function Artist(id, name, releases = [], featureReleases = []) {
  let _id = id;
  let _name = name;
  let _releases = releases;
  let _featureReleases = featureReleases;

  function addRelease(newRelease) {
    newRelease.setArtist(instance);
    let release = _releases.find((release) => release.getId() === newRelease.getId());
    if (!release) {
      _releases.push(newRelease);
    } else {
      release.addTracks(newRelease.getTracks());
    }
  }

  function addReleases(releases) {
    releases.forEach((release) => addRelease(release));
  }

  function addFeatureRelease(newRelease) {
    if (
      _featureReleases.findIndex(
        (release) => release.getId() === newRelease.getId()
      ) === -1
    ) {
      newRelease.addFeatureArtist(instance);
      _featureReleases.push(newRelease);
    }
  }

  function addFeatureReleases(releases) {
    releases.forEach((release) => addFeatureRelease(release));
  }

  function complement(artist) {
    const complementedArtist = new Artist(
      _id || artist.getId(),
      _name || artist.getName(),
      _releases,
      _featureReleases
    );
    complementedArtist.addReleases(artist.getReleases());
    complementedArtist.addFeatureReleases(artist.getFeatureReleases());
    return complementedArtist;
  }

  function toString() {
    return (
      `Artist {` +
      `name: ${_name}, ` +
      `releases: ${_releases.map((release) => release.getTitle())}, ` +
      `featureReleases: ${_featureReleases.map((release) => release.getTitle())}, ` +
      `id: ${_id}}`
    );
  }

  function getName() {
    return _name;
  }

  function setName(value) {
    _name = value;
    return instance;
  }

  function getReleases() {
    return _releases;
  }

  function setReleases(value) {
    _releases = value;
    return instance;
  }

  function getId() {
    return _id;
  }

  function setId(value) {
    _id = value;
    return instance;
  }

  function getFeatureReleases() {
    return _featureReleases;
  }

  function setFeatureReleases(value) {
    _featureReleases = value;
    return instance;
  }

  let instance = {
    addRelease,
    addReleases,
    addFeatureRelease,
    addFeatureReleases,
    complement,
    toString,
    getName,
    setName,
    getReleases,
    setReleases,
    getId,
    setId,
    getFeatureReleases,
    setFeatureReleases,
  };

  return instance;
}

module.exports = Artist;
