const GmidGenerator = require("gmid-generator");

const DirectoryScanner = require("./src/files/directory-scanner");
const TagReader = require("./src/media/tag-reader");
const Artist = require("./src/media/artist");
const Release = require("./src/media/release");
const ReleaseCover = require("./src/media/release-cover");
const Track = require("./src/media/track");
const { MP3, FLAC } = require("./src/files/file");
const Logger = require("./src/utils/logger");
const { DEBUG, INFO, WARN, ERROR, FATAL, OFF } = Logger;
const EventBus = require("./src/core/event-bus");
const Events = require("./src/core/events");
const { ARTIST_ADDED, RELEASE_ADDED, TRACK_ADDED, FILE_ADDED } = Events;

const logger = Logger.createNamespace("MelLibrary");

function MelLibrary() {
  let _directories = [];
  let _files = [];
  let _artists = [];
  let _releases = [];
  let _tracks = [];
  let _releaseCovers = [];

  let _fileSystem;
  let _extensions;

  let _directoryScanner = new DirectoryScanner();
  let _tagReader = new TagReader();

  function setConfig({ fileSystem, extensions, logLevel } = {}) {
    _fileSystem = fileSystem;
    _extensions = extensions;
    if (isNaN(logLevel)) logLevel = INFO;

    Logger.setLogLevel(logLevel);

    _directoryScanner.setConfig({
      fileSystem: _fileSystem,
      extensions,
    });

    _tagReader.setConfig({
      fileSystem: _fileSystem,
    });
  }

  function addDirectory(directory) {
    _directories.push(directory);
  }

  async function scanDirectories() {
    logger.info("Starting to scan local directories");
    await _directoryScanner.scanDirs(_directories, (file) => addFile(file));
    logger.debug("Finished scraping music files");
    for (let file of _files) {
      logger.debug("Reading tags of file: " + file.getPath());
      const metadata = await _tagReader.readTags(file);
      logger.debug("Finished reading tags of file: " + file.getPath());
      const artistId = GmidGenerator.generateArtistGmid(metadata.artistName);
      const artist = new Artist(artistId, metadata.artistName);
      addArtist(artist);
      const albumArtistId = GmidGenerator.generateArtistGmid(
        metadata.albumArtistName
      );
      const albumArtist = new Artist(albumArtistId, metadata.albumArtistName);
      addArtist(albumArtist);
      const releaseId = GmidGenerator.generateReleaseGmid(
        metadata.albumTitle,
        metadata.albumArtistName
      );
      const release = new Release(
        releaseId,
        albumArtist,
        metadata.albumTitle,
        metadata.year
      );
      addRelease(release);
      const trackId = GmidGenerator.generateTrackGmid(
        metadata.trackTitle,
        metadata.trackNumber,
        metadata.albumTitle,
        metadata.albumArtistName
      );
      const track = new Track(
        trackId,
        metadata.trackTitle,
        null,
        release,
        metadata.trackNumber,
        metadata.discNumber
      );
      addTrack(track);

      if (getReleaseCover(release.getId())) continue;
      const albumCover = await _tagReader.readAlbumCover(file);
      const releaseCover = new ReleaseCover(
        release.getId(),
        albumCover.data,
        albumCover.format
      );
      addReleaseCover(releaseCover);
    }
  }

  function addArtist(newArtist) {
    const index = _artists.findIndex(
      (artist) => artist.getId() === newArtist.getId()
    );
    if (index !== -1) return;
    _artists.push(newArtist);
    logger.info("Added artist " + newArtist.toString());
    EventBus.dispatch(ARTIST_ADDED);
  }

  function addRelease(newRelease) {
    const index = _releases.findIndex(
      (release) => release.getId() === newRelease.getId()
    );
    if (index !== -1) return;
    _releases.push(newRelease);
    logger.info("Added release " + newRelease.toString());
    EventBus.dispatch(RELEASE_ADDED);
  }

  function addTrack(newTrack) {
    const index = _tracks.findIndex(
      (track) => track.getId() === newTrack.getId()
    );
    if (index !== -1) return;
    _tracks.push(newTrack);
    logger.info("Added track " + newTrack.toString());
    EventBus.dispatch(TRACK_ADDED);
  }

  function addFile(newFile) {
    const index = _files.findIndex(
      (file) => file.getPath() === newFile.getPath()
    );
    if (index !== -1) return;
    _files.push(newFile);
    logger.info("Added file " + newFile.toString());
    EventBus.dispatch(FILE_ADDED);
  }

  function addReleaseCover(newReleaseCover) {
    const index = _releaseCovers.findIndex(
      (releaseCover) =>
        releaseCover.getReleaseId() === newReleaseCover.getReleaseId()
    );
    if (index !== -1) return;
    _releaseCovers.push(newReleaseCover);
    logger.info(
      "Added release cover for release " + newReleaseCover.getReleaseId()
    );
  }

  function getFiles() {
    return _files;
  }

  function getDirectories() {
    return _directories;
  }

  function getArtists() {
    return _artists;
  }

  function getReleases() {
    return _releases;
  }

  function getReleaseCover(releaseId) {
    return _releaseCovers.find(
      (releaseCover) => releaseCover.getReleaseId() === releaseId
    );
  }

  function getTracks() {
    return _tracks;
  }

  return {
    setConfig,
    addDirectory,
    scanDirectories,
    getFiles,
    getDirectories,
    getArtists,
    getReleases,
    getReleaseCover,
    getTracks,
    on: EventBus.on,
    off: EventBus.off,
  };
}

MelLibrary.files = {
  MP3,
  FLAC,
};

MelLibrary.logging = {
  DEBUG,
  INFO,
  WARN,
  ERROR,
  FATAL,
  OFF,
};

MelLibrary.events = Events;

module.exports = MelLibrary;
