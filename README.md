# Mel Library

## Dependencies

[jsmediatags](https://github.com/aadsm/jsmediatags)  
Reading metadata from music files.

[js-crc](https://github.com/emn178/js-crc)  
Generating CRC checksums.
