# Mel Library API - [Mel Library Documentation](./README.md)

## Contents

- [1. Constructor](#1-constructor)
- [2. Instance Methods](#2-instance-methods)
  - [2.1 setConfig()](#21-setconfig)
  - [2.2 addDirectory()](#22-adddirectory)
  - [2.3 scanDirectories()](#23-scandirectories)
  - [2.4 on()](#24-on)
  - [2.5 off()](#25-off)
- [3. Constants](#3-constants)
  - [3.1 Files](#31-files)
  - [3.2 Events](#32-events)
  - [3.3 Logging](#33-logging)

## 1. Constructor

```js
MelLibrary();
```

Creates a new `MelLibrary` object.

## 2. Instance Methods

### 2.1 setConfig()

Sets the configuration for the `MelLibrary` instance.

#### Syntax

```js
melLibrary.setConfig(configuration);
```

##### Parameters

**configuration**  
The configuration contains various parameters which defines how the `MelLibrary` behaves.

```js
{
  fileSystem: FileSystemInterface,
  extensions: [],
  logLevel: INFO
}
```

- **fileSystem**: An implementation of the [file system interface](./file-system.md)
- **extensions**: An array of file extensions to look for. See [file constants](#files)
- **logLevel**: Sets the global log level. See [logging constants](#logging)

##### Return value

none

### 2.2 addDirectory()

Used to add local directories to scan for music.

#### Syntax

```js
melLibrary.addDirectory(directoryPath);
```

##### Parameters

**directoryPath**  
The path to the directory that contains music files.

##### Return value

none

### 2.3 scanDirectories()

Scans all configured local directories for new and updated music files.

#### Syntax

```js
melLibrary.scanDirectories();
```

##### Parameters

none

##### Return value

A `Promise`

### 2.4 on()

Used to register a handler for a specific event.

#### Syntax

```js
melLibrary.on(eventName, handler);
```

##### Parameter

**eventName**  
The name of the event to register for. Please see [event constants](#events)

**handler**  
The function that is called once the specified event occurs.

##### Return value

none

### 2.5 off()

Used to unregister an event handler.

#### Syntax

```js
melLibrary.off(handler);
```

##### Parameters

**handler**  
The callback function to unregister.

##### Return value

none

## 3. Constants

### 3.1 Files

```js
MelLibrary.files;
```

| Constant | Description                |
| -------- | -------------------------- |
| `MP3`    | Used to define mp3 files.  |
| `FLAC`   | Used to define flac files. |

### 3.2 Events

```js
MelLibrary.events;
```

| Constant                  | Description                               |
| ------------------------- | ----------------------------------------- |
| `DIRECTORY_SCAN_STARTED`  | Fired when a directory scan is initiated. |
| `DIRECTORY_SCAN_FINISHED` | Fired when a directory scan is completed. |
| `ARTIST_ADDED`            | Fired when a new artist was added.        |
| `RELEASE_ADDED`           | Fired when a new release was added.       |
| `TRACK_ADDED`             | Fired when a new track was added.         |
| `FILE_ADDED`              | Fired when a new file was added.          |

### 3.3 Logging

```js
MelLibrary.logging;
```

| Constant | Description                                   |
| -------- | --------------------------------------------- |
| `OFF`    | Logging level. Turns off logging.             |
| `FATAL`  | Only log message with level 'FATAL' or above. |
| `ERROR`  | Only log message with level 'ERROR' or above. |
| `WARN`   | Only log message with level 'WARN' or above.  |
| `INFO`   | Only log message with level 'INFO' or above.  |
| `DEBUG`  | Only log message with level 'DEBUG' or above. |
