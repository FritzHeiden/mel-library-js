# File System Interface - [Mel Library Documentation](./README.md)

## 1. Instance Methods

### 1.1 readDir()

Reads the files contained in a specific directory.

#### Syntax

```js
fileSystem.readDir(directoryPath);
```

##### Parameters

**directoryPath**  
Path to the directory to read

##### Return value

Array of file names of the files contained in the specified directory.

### 1.2 stats()

Read various stats of a specific file or directory.

#### Syntax

```js
fileSystem.stats(path);
```

##### Parameters

**path**  
The path to the file or directory to read stats from.

##### Return value

An object containing various stats:

```js
{
  isDirectory: Boolean,
  lastModified: Number,
  size: Number
}
```

- **isDirectory**: Whether or not the path is a directory
- **lastModified**: The time the file was last modified in Unix Epoch milliseconds
- **size**: The size of the file in bytes

### 1.3 read()

Reads a range of bytes from a specified file.

#### Syntax

```js
fileSystem.read(filePath, range);
```

##### Parameters

**filePath**  
The path to the file to read from

**range**  
The range of bytes to read from the file

```js
{
  "start": Number,
  "end": Number
}
```

##### Return value

A DataView containing the data.
