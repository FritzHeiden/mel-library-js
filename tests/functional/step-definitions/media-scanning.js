const path = require("path");
const assert = require("assert");
const { Given, When, Then } = require("@cucumber/cucumber");
const { crc32 } = require("js-crc");

const MelLibrary = require("../../../index.js");
const { MP3, FLAC } = MelLibrary.files;
const { OFF } = MelLibrary.logging;
const NodeJsFileSystem = require("../../../modules/node-js-file-system");

const TEST_FILES_DIRECTORY = path.resolve("./tests/test-files");

Given("a valid directory path was provided", function () {
  this.melLibrary = new MelLibrary();
  this.melLibrary.setConfig({ logLevel: OFF });
  this.melLibrary.addDirectory(TEST_FILES_DIRECTORY);
});

Given("configuration is set", function () {
  const fileSystem = new NodeJsFileSystem();
  this.melLibrary.setConfig({
    fileSystem,
    extensions: [MP3, FLAC],
    logLevel: OFF,
  });
});

Given("configuration is set to only scan MP3 files", function () {
  const fileSystem = new NodeJsFileSystem();
  this.melLibrary.setConfig({ fileSystem, extensions: [MP3], logLevel: OFF });
});

Given("configuration is set to only scan FLAC files", function () {
  const fileSystem = new NodeJsFileSystem();
  this.melLibrary.setConfig({ fileSystem, extensions: [FLAC], logLevel: OFF });
});

Given("no directories were provided", function () {
  this.melLibrary = new MelLibrary();
  this.melLibrary.setConfig({ logLevel: OFF });
});

Given("time is measured", function () {
  this.startTime = Date.now();
});

When("a complete scan is performed", async function () {
  await this.melLibrary.scanDirectories();
});

When("a directory is added", function () {
  this.melLibrary.addDirectory(TEST_FILES_DIRECTORY);
});

When("a complete scan is performed {int}x", async function (scanRepetitions) {
  this.iterations = scanRepetitions;
  for (let i = 0; i < scanRepetitions; i++) {
    await this.melLibrary.scanDirectories();
  }
});

Then("the scanned files can be returned", function () {
  const files = this.melLibrary.getFiles();

  assert.strictEqual(files instanceof Array, true);
  assert.strictEqual(files.length, 2);
});

Then("the scanned MP3 files can be returned", function () {
  const files = this.melLibrary.getFiles();

  assert.strictEqual(files instanceof Array, true);
  assert.strictEqual(files.length, 1);
});

Then("the scanned FLAC files can be returned", function () {
  const files = this.melLibrary.getFiles();

  assert.strictEqual(files instanceof Array, true);
  assert.strictEqual(files.length, 1);
});

Then("the directory can be returned", function () {
  const directories = this.melLibrary.getDirectories();

  assert.strictEqual(directories instanceof Array, true);
  assert.strictEqual(directories.length, 1);
  assert.strictEqual(directories[0], TEST_FILES_DIRECTORY);
});

Then("the music files meta data can be returned", function () {
  const artists = this.melLibrary.getArtists();
  const releases = this.melLibrary.getReleases();
  const tracks = this.melLibrary.getTracks();

  assert.strictEqual(
    artists instanceof Array,
    true,
    "Array of artists returned"
  );
  assert.strictEqual(artists.length, 2, "Total of 2 artists returned");
  const artist = artists.find((artist) => artist.getName() === "Test Artist");
  const albumArtist = artists.find(
    (artist) => artist.getName() === "Test Album Artist"
  );

  assert.strictEqual(!!artist, true, "Returned artists contain 'Test Artist'");
  assert.strictEqual(
    !!albumArtist,
    true,
    "Returned artists contain 'Test Album Artist'"
  );

  assert.strictEqual(
    releases instanceof Array,
    true,
    "Array of releases returned"
  );
  assert.strictEqual(releases.length, 1, "Total of 1 release returned");
  const release = releases[0];
  assert.strictEqual(
    release.getTitle(),
    "Test Album",
    "Album has title 'Test Album'"
  );
  assert.strictEqual(release.getYear(), "2020", "Album has year '2020'");
  assert.strictEqual(
    release.getArtist().getName(),
    "Test Album Artist",
    "Release as artist"
  );

  assert.strictEqual(tracks instanceof Array, true, "Array of tracks returned");
  assert.strictEqual(tracks.length, 1, "Total of 1 track returned");
  assert.strictEqual(
    tracks[0].getTitle(),
    "Test Title",
    "Track has title 'Test Title'"
  );
  assert.strictEqual(tracks[0].getNumber(), "1", "Track has number '1'");
  assert.strictEqual(
    tracks[0].getDiscNumber(),
    "1",
    "Track has disc number '1'"
  );
});

Then("the artist, release and track objects have correct IDs", function () {
  const artists = this.melLibrary.getArtists();
  const artist = artists.find((artist) => artist.getName() === "Test Artist");
  const albumArtist = artists.find(
    (artist) => artist.getName() === "Test Album Artist"
  );
  const releases = this.melLibrary.getReleases();
  const release = releases[0];
  const tracks = this.melLibrary.getTracks();
  const track = tracks[0];

  const artistId = artist.getName().toLowerCase().replace(/ /g, "");
  const albumArtistId = albumArtist.getName().toLowerCase().replace(/ /g, "");
  const releaseId =
    release.getTitle().toLowerCase().replace(/ /g, "") + ":" + albumArtistId;
  const trackId =
    (track.getNumber() + "").padStart(3, "0") +
    track.getTitle().toLowerCase().replace(/ /g, "") +
    ":" +
    releaseId;

  assert.strictEqual(artist.getId(), artistId, "Artist ID is correct");
  assert.strictEqual(
    albumArtist.getId(),
    albumArtistId,
    "Album artist ID is correct"
  );
  assert.strictEqual(release.getId(), releaseId, "Release ID is correct");
  assert.strictEqual(track.getId(), trackId, "Track ID is correct");
});

Then("release cover can be returned", function () {
  const releases = this.melLibrary.getReleases();

  for (let release of releases) {
    const releaseCover = this.melLibrary.getReleaseCover(release.getId());
    assert.strictEqual(!!releaseCover, true, "A truthy value was returned");
    assert.strictEqual(
      !!releaseCover.getReleaseId,
      true,
      "Release cover has getReleaseId function"
    );
    assert.strictEqual(
      releaseCover.getReleaseId(),
      release.getTitle().toLowerCase().replace(/ /g, "") +
        ":" +
        release.getArtist().getName().toLowerCase().replace(/ /g, ""),
      "Release cover has correct release id"
    );
    assert.strictEqual(
      !!releaseCover.getType,
      true,
      "Release cover has getType function"
    );
    assert.strictEqual(
      releaseCover.getType(),
      "image/jpeg",
      "Release cover has correct type"
    );
    assert.strictEqual(
      !!releaseCover.getBuffer,
      true,
      "Release cover has getBuffer function"
    );
    const crc = crc32(releaseCover.getBuffer());
    assert.strictEqual(
      crc,
      "4b76cf84",
      "Release cover has correct crc checksum"
    );
  }
});

Then("the measured time is sufficiently low", function () {
  const duration = Date.now() - this.startTime;
  assert.strictEqual(duration < this.iterations * 3, true, "below 3ms per iteration");
});
