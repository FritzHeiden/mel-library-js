const { Given, When, Then } = require("@cucumber/cucumber");
const path = require("path");
const assert = require("assert");

const MelLibrary = require("../../../index");
const NodeJsFileSystem = require("../../../modules/node-js-file-system");

const { MP3, FLAC } = MelLibrary.files;
const { DEBUG, OFF } = MelLibrary.logging;
const {
  DIRECTORY_SCAN_STARTED,
  DIRECTORY_SCAN_FINISHED,
  ARTIST_ADDED,
  RELEASE_ADDED,
  TRACK_ADDED,
  FILE_ADDED,
} = MelLibrary.events;

const TEST_FILES_DIRECTORY = path.resolve("./tests/test-files");

Given("the system is configured", function () {
  this.melLibrary = new MelLibrary();
  const fileSystem = new NodeJsFileSystem();
  this.melLibrary.setConfig({
    fileSystem,
    extensions: [MP3, FLAC],
    logLevel: OFF,
  });
  this.melLibrary.addDirectory(TEST_FILES_DIRECTORY);
});

Given(
  "an event handler for directory scan started was registered",
  function () {
    let firedEvents = this.firedEvents;
    if (!firedEvents) {
      firedEvents = [];
      this.firedEvents = firedEvents;
    }

    this.melLibrary.on(DIRECTORY_SCAN_STARTED, () =>
      firedEvents.push(DIRECTORY_SCAN_STARTED)
    );
  }
);

Given(
  "an event handler for directory scan finished was registered",
  function () {
    let firedEvents = this.firedEvents;
    if (!firedEvents) {
      firedEvents = [];
      this.firedEvents = firedEvents;
    }

    this.melLibrary.on(DIRECTORY_SCAN_FINISHED, () =>
      firedEvents.push(DIRECTORY_SCAN_FINISHED)
    );
  }
);

Given("an event handler for artist added was registered", function () {
  let firedEvents = this.firedEvents;
  if (!firedEvents) {
    firedEvents = [];
    this.firedEvents = firedEvents;
  }

  this.melLibrary.on(ARTIST_ADDED, () => firedEvents.push(ARTIST_ADDED));
});

Given("an event handler for release added was registered", function () {
  let firedEvents = this.firedEvents;
  if (!firedEvents) {
    firedEvents = [];
    this.firedEvents = firedEvents;
  }

  this.melLibrary.on(RELEASE_ADDED, () => firedEvents.push(RELEASE_ADDED));
});

Given("an event handler for track added was registered", function () {
  let firedEvents = this.firedEvents;
  if (!firedEvents) {
    firedEvents = [];
    this.firedEvents = firedEvents;
  }

  this.melLibrary.on(TRACK_ADDED, () => firedEvents.push(TRACK_ADDED));
});

Given("an event handler for file added was registered", function () {
  let firedEvents = this.firedEvents;
  if (!firedEvents) {
    firedEvents = [];
    this.firedEvents = firedEvents;
  }

  this.melLibrary.on(FILE_ADDED, () => firedEvents.push(FILE_ADDED));
});

When("a directory scan is performed", async function () {
  await this.melLibrary.scanDirectories();
});

Then("the directory scan started event was received", function () {
  assert.strictEqual(
    this.firedEvents.indexOf(DIRECTORY_SCAN_STARTED) !== -1,
    true,
    "Directory scan started event was fired"
  );
});

Then("the directory scan finished event was received", function () {
  assert.strictEqual(
    this.firedEvents.indexOf(DIRECTORY_SCAN_FINISHED) !== -1,
    true,
    "Directory scan finished event was fired"
  );
});

Then("the artist added event was received", function () {
  assert.strictEqual(
    this.firedEvents.indexOf(ARTIST_ADDED) !== -1,
    true,
    "Artist added event was fired"
  );
});

Then("the release added event was received", function () {
  assert.strictEqual(
    this.firedEvents.indexOf(RELEASE_ADDED) !== -1,
    true,
    "Release added event was fired"
  );
});

Then("the track added event was received", function () {
  assert.strictEqual(
    this.firedEvents.indexOf(TRACK_ADDED) !== -1,
    true,
    "Track added event was fired"
  );
});

Then("the file added event was received", function () {
  assert.strictEqual(
    this.firedEvents.indexOf(FILE_ADDED) !== -1,
    true,
    "File added event was fired"
  );
});
