Feature: Registering for various system events
  It shall be possible to register event handlers for all relevant system events

  Scenario: Receive event on directory scan started
    Given the system is configured
    And an event handler for directory scan started was registered
    When a directory scan is performed
    Then the directory scan started event was received

  Scenario: Receive event on directory scan finished
    Given the system is configured
    And an event handler for directory scan finished was registered
    When a directory scan is performed
    Then the directory scan finished event was received

  Scenario: Receive event on artist added
    Given the system is configured
    And an event handler for artist added was registered
    When a directory scan is performed
    Then the artist added event was received

  Scenario: Receive event on release added
    Given the system is configured
    And an event handler for release added was registered
    When a directory scan is performed
    Then the release added event was received

  Scenario: Receive event on track added
    Given the system is configured
    And an event handler for track added was registered
    When a directory scan is performed
    Then the track added event was received

  Scenario: Receive event on file added
    Given the system is configured
    And an event handler for file added was registered
    When a directory scan is performed
    Then the file added event was received
