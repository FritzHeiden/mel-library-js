const ReleaseCover = require("../../src/media/release-cover");


test("data passed to constructor is retrievable by getters", () => {
  const RELEASE_ID = "A";
  const BUFFER = new ArrayBuffer();
  const TYPE = "image/jpeg";
  const releaseCover = new ReleaseCover(RELEASE_ID, BUFFER, TYPE);

  expect(releaseCover).toHaveProperty("getReleaseId");
  expect(releaseCover.getReleaseId).toBeInstanceOf(Function);
  expect(releaseCover.getReleaseId()).toBe(RELEASE_ID);

  expect(releaseCover).toHaveProperty("getBuffer");
  expect(releaseCover.getBuffer).toBeInstanceOf(Function);
  expect(releaseCover.getBuffer()).toBe(BUFFER);

  expect(releaseCover).toHaveProperty("getType");
  expect(releaseCover.getType).toBeInstanceOf(Function);
  expect(releaseCover.getType()).toBe(TYPE);
});


test("using setters changes values returned by getters accordingly", () => {
  const RELEASE_ID = "A";
  const BUFFER = new ArrayBuffer();
  const TYPE = "image/jpeg";
  const releaseCover = new ReleaseCover();

  expect(releaseCover.getReleaseId()).not.toBe(RELEASE_ID);
  releaseCover.setReleaseId(RELEASE_ID);
  expect(releaseCover.getReleaseId()).toBe(RELEASE_ID);

  expect(releaseCover.getBuffer()).not.toBe(BUFFER);
  releaseCover.setBuffer(BUFFER);
  expect(releaseCover.getBuffer()).toBe(BUFFER);

  expect(releaseCover.getType()).not.toBe(TYPE);
  releaseCover.setType(TYPE);
  expect(releaseCover.getType()).toBe(TYPE);
})
