const Artist = require("../../src/media/artist");
const Release = require("../../src/media/release");

test("data passed to constructor is retrievable by getters", () => {
  const ARTIST_ID = "artist_id";
  const ARTIST_NAME = "artist_name";
  const RELEASES = [new Release("", null, "", "")];
  const FEATURE_RELEASES = [new Release("", null, "", "")];
  const artist = new Artist(ARTIST_ID, ARTIST_NAME, RELEASES, FEATURE_RELEASES);

  expect(artist).toHaveProperty("getId");
  expect(artist.getId).toBeInstanceOf(Function);
  expect(artist.getId()).toBe(ARTIST_ID);

  expect(artist).toHaveProperty("getName");
  expect(artist.getName).toBeInstanceOf(Function);
  expect(artist.getName()).toBe(ARTIST_NAME);

  expect(artist).toHaveProperty("getReleases");
  expect(artist.getReleases).toBeInstanceOf(Function);
  expect(artist.getReleases()).toEqual(expect.arrayContaining(RELEASES));

  expect(artist).toHaveProperty("getFeatureReleases");
  expect(artist.getFeatureReleases).toBeInstanceOf(Function);
  expect(artist.getFeatureReleases()).toEqual(
    expect.arrayContaining(FEATURE_RELEASES)
  );
});

test("using setters changes values returned by getters accordingly", () => {
  const ARTIST_ID = "artist_id";
  const ARTIST_NAME = "artist_name";
  const RELEASES = [new Release("", null, "", "")];
  const FEATURE_RELEASES = [new Release("", null, "", "")];
  const artist = new Artist();

  expect(artist.getId()).not.toBe(ARTIST_ID);
  artist.setId(ARTIST_ID);
  expect(artist.getId()).toBe(ARTIST_ID);

  expect(artist.getName()).not.toBe(ARTIST_NAME);
  artist.setName(ARTIST_NAME);
  expect(artist.getName()).toBe(ARTIST_NAME);

  expect(artist.getReleases()).not.toEqual(expect.arrayContaining(RELEASES));
  artist.setReleases(RELEASES);
  expect(artist.getReleases()).toEqual(expect.arrayContaining(RELEASES));

  expect(artist.getFeatureReleases()).not.toEqual(
    expect.arrayContaining(FEATURE_RELEASES)
  );
  artist.setFeatureReleases(FEATURE_RELEASES);
  expect(artist.getFeatureReleases()).toEqual(
    expect.arrayContaining(FEATURE_RELEASES)
  );
});

test("toString() returns data as string", () => {
  const ARTIST_ID = "artist_id";
  const ARTIST_NAME = "artist_name";
  const RELEASES = [new Release("", null, "release", "")];
  const FEATURE_RELEASES = [new Release("", null, "feature_release", "")];
  const artist = new Artist(ARTIST_ID, ARTIST_NAME, RELEASES, FEATURE_RELEASES);

  expect(artist.toString()).toEqual(
    "Artist {name: artist_name, releases: release, featureReleases: feature_release, id: artist_id}"
  );
});

test("addRelease adds a release to artist, artist of release is changed accordingly", () => {
  const RELEASE = new Release();

  let artist = new Artist();

  expect(RELEASE.getArtist()).not.toEqual(artist);
  expect(artist.getReleases()).not.toContain(RELEASE);
  artist.addRelease(RELEASE);
  expect(artist.getReleases()).toContain(RELEASE);
  expect(RELEASE.getArtist()).toEqual(artist);
});

test("addReleases adds multiple releases to artist, all releases artists get changed accordingly", () => {
  const RELEASE_A = new Release("A");
  const RELEASE_B = new Release("B");

  let artist = new Artist();

  expect(RELEASE_A.getArtist()).not.toEqual(artist);
  expect(RELEASE_B.getArtist()).not.toEqual(artist);
  expect(artist.getReleases()).not.toContain(RELEASE_A);
  expect(artist.getReleases()).not.toContain(RELEASE_B);
  artist.addReleases([RELEASE_A, RELEASE_B]);
  expect(artist.getReleases()).toContain(RELEASE_A);
  expect(artist.getReleases()).toContain(RELEASE_B);
  expect(RELEASE_A.getArtist()).toEqual(artist);
  expect(RELEASE_B.getArtist()).toEqual(artist);
});

test("addFeatureRelease adds a feature release to artist", () => {
  const RELEASE = new Release();

  let artist = new Artist();

  expect(artist.getFeatureReleases()).not.toContain(RELEASE);
  artist.addFeatureRelease(RELEASE);
  expect(artist.getFeatureReleases()).toContain(RELEASE);
});

test("addFeatureReleases adds multiple feature releases to artist", () => {
  const RELEASE_A = new Release("A");
  const RELEASE_B = new Release("B");

  let artist = new Artist();

  expect(artist.getFeatureReleases()).not.toContain(RELEASE_A);
  expect(artist.getFeatureReleases()).not.toContain(RELEASE_B);
  artist.addFeatureReleases([RELEASE_A, RELEASE_B]);
  expect(artist.getFeatureReleases()).toContain(RELEASE_A);
  expect(artist.getFeatureReleases()).toContain(RELEASE_B);
});

test("complement adds missing data from provided artist", () => {
  const RELEASE_A = new Release("A");
  const RELEASE_B = new Release("B");
  const ID = "id";
  const NAME = "name";
  let artist = new Artist();
  let complementingArtist = new Artist(ID, NAME, [RELEASE_A], [RELEASE_B]);

  expect(artist.getId()).not.toEqual(ID);
  expect(artist.getName()).not.toEqual(NAME);
  expect(artist.getReleases()).not.toContain(RELEASE_A);
  expect(artist.getFeatureReleases()).not.toContain(RELEASE_B);
  artist = artist.complement(complementingArtist);
  expect(artist.getId()).toEqual(ID);
  expect(artist.getName()).toEqual(NAME);
  expect(artist.getReleases()).toContain(RELEASE_A);
  expect(artist.getFeatureReleases()).toContain(RELEASE_B);
});

test("complement doesnt overwrite existing data", () => {
  const RELEASE_A = new Release("A");
  const RELEASE_B = new Release("B");
  const ID_A = "ida";
  const ID_B = "idb";
  const NAME_A = "namea";
  const NAME_B = "nameb";
  let artist = new Artist(ID_A, NAME_A, [RELEASE_A], [RELEASE_B]);
  let complementingArtist = new Artist(ID_B, NAME_B, [RELEASE_B], [RELEASE_A]);

  expect(artist.getId()).toEqual(ID_A);
  expect(artist.getName()).toEqual(NAME_A);
  expect(artist.getReleases()).toContain(RELEASE_A);
  expect(artist.getReleases()).not.toContain(RELEASE_B);
  expect(artist.getFeatureReleases()).not.toContain(RELEASE_A);
  expect(artist.getFeatureReleases()).toContain(RELEASE_B);
  artist = artist.complement(complementingArtist);
  expect(artist.getId()).toEqual(ID_A);
  expect(artist.getName()).toEqual(NAME_A);
  expect(artist.getReleases()).toContain(RELEASE_A);
  expect(artist.getReleases()).toContain(RELEASE_B);
  expect(artist.getFeatureReleases()).toContain(RELEASE_A);
  expect(artist.getFeatureReleases()).toContain(RELEASE_B);
});
