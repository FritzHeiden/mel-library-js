const Track = require("../../src/media/track");
const Artist = require("../../src/media/artist");
const Release = require("../../src/media/release");

test("data passed to constructor is retrievable by getters", () => {
  const ID = "id";
  const TITLE = "track";
  const ARTISTS = [new Artist()];
  const RELEASE = new Release();
  const NUMBER = 3;
  const DISC_NUMBER = 2;

  let track = new Track(ID, TITLE, ARTISTS, RELEASE, NUMBER, DISC_NUMBER);

  expect(track).toHaveProperty("getId");
  expect(track.getId).toBeInstanceOf(Function);
  expect(track.getId()).toBe(ID);

  expect(track).toHaveProperty("getTitle");
  expect(track.getTitle).toBeInstanceOf(Function);
  expect(track.getTitle()).toBe(TITLE);

  expect(track).toHaveProperty("getArtists");
  expect(track.getArtists).toBeInstanceOf(Function);
  expect(track.getArtists()).toBe(ARTISTS);

  expect(track).toHaveProperty("getRelease");
  expect(track.getRelease).toBeInstanceOf(Function);
  expect(track.getRelease()).toBe(RELEASE);

  expect(track).toHaveProperty("getNumber");
  expect(track.getNumber).toBeInstanceOf(Function);
  expect(track.getNumber()).toBe(NUMBER);

  expect(track).toHaveProperty("getDiscNumber");
  expect(track.getDiscNumber).toBeInstanceOf(Function);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER);
});

test("using setters changes values returned by getters accordingly", () => {
  const ID = "id";
  const TITLE = "track";
  const ARTISTS = [new Artist()];
  const RELEASE = new Release();
  const NUMBER = 3;
  const DISC_NUMBER = 2;

  let track = new Track();

  expect(track.getId()).not.toBe(ID);
  track.setId(ID);
  expect(track.getId()).toBe(ID);

  expect(track.getTitle()).not.toBe(TITLE);
  track.setTitle(TITLE);
  expect(track.getTitle()).toBe(TITLE);

  expect(track.getArtists()).not.toBe(ARTISTS);
  track.setArtists(ARTISTS);
  expect(track.getArtists()).toBe(ARTISTS);

  expect(track.getRelease()).not.toBe(RELEASE);
  track.setRelease(RELEASE);
  expect(track.getRelease()).toBe(RELEASE);

  expect(track.getNumber()).not.toBe(NUMBER);
  track.setNumber(NUMBER);
  expect(track.getNumber()).toBe(NUMBER);

  expect(track.getDiscNumber()).not.toBe(DISC_NUMBER);
  track.setDiscNumber(DISC_NUMBER);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER);
});

test("toString() returns data as string", () => {
  const ID = "id";
  const TITLE = "track";
  const ARTISTS = [new Artist()];
  const RELEASE = new Release();
  const NUMBER = 3;
  const DISC_NUMBER = 2;

  let track = new Track(ID, TITLE, ARTISTS, RELEASE, NUMBER, DISC_NUMBER);

  expect(track.toString()).toEqual(
    "Track {title: track, artists: , release: undefined, number: 3, discNumber: 2, id: id}"
  );
});

test("addArtist adds an artist to the track", () => {
  const ID = "id";
  const TITLE = "track";
  const ARTIST = new Artist("A");
  const ARTISTS = [new Artist("B")];
  const RELEASE = new Release();
  const NUMBER = 3;
  const DISC_NUMBER = 2;

  let track = new Track(ID, TITLE, ARTISTS, RELEASE, NUMBER, DISC_NUMBER);

  expect(track.getArtists()).not.toContain(ARTIST);
  track.addArtist(ARTIST);
  expect(track.getArtists()).toContain(ARTIST);
});

test("addArtists adds multiple artists to the track", () => {
  const ID = "id";
  const TITLE = "track";
  const NEW_ARTISTS = [new Artist("A"), new Artist("B")];
  const ARTISTS = [new Artist("C")];
  const RELEASE = new Release();
  const NUMBER = 3;
  const DISC_NUMBER = 2;

  let track = new Track(ID, TITLE, ARTISTS, RELEASE, NUMBER, DISC_NUMBER);

  expect(track.getArtists()).not.toEqual(expect.arrayContaining(NEW_ARTISTS));
  track.addArtists(NEW_ARTISTS);
  expect(track.getArtists()).toEqual(expect.arrayContaining(NEW_ARTISTS));
});

test("complement adds missing data from provided track", () => {
  const ID = "id";
  const TITLE = "track";
  const ARTISTS = [new Artist("C")];
  const RELEASE = new Release();
  const NUMBER = 3;
  const DISC_NUMBER = 2;

  let track = new Track();
  let complementingTrack = new Track(ID, TITLE, ARTISTS, RELEASE, NUMBER, DISC_NUMBER);

  expect(track.getId()).not.toBe(ID);
  expect(track.getTitle()).not.toBe(TITLE);
  expect(track.getArtists()).not.toEqual(expect.arrayContaining(ARTISTS));
  expect(track.getRelease()).not.toBe(RELEASE);
  expect(track.getNumber()).not.toBe(NUMBER);
  expect(track.getDiscNumber()).not.toBe(DISC_NUMBER);
  track = track.complement(complementingTrack);
  expect(track.getId()).toBe(ID);
  expect(track.getTitle()).toBe(TITLE);
  expect(track.getArtists()).toEqual(expect.arrayContaining(ARTISTS));
  expect(track.getRelease()).toEqual(RELEASE);
  expect(track.getNumber()).toBe(NUMBER);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER);
});

test("complement doesnt overwrite existing data", () => {
  const ID_A = "idA";
  const ID_B = "idB";
  const TITLE_A = "trackA";
  const TITLE_B = "trackB";
  const ARTISTS_A = [new Artist("A")];
  const ARTISTS_B = [new Artist("B")];
  const RELEASE_A = new Release("A");
  const RELEASE_B = new Release("B");
  const NUMBER_A = 3;
  const NUMBER_B = 5;
  const DISC_NUMBER_A = 2;
  const DISC_NUMBER_B = 1;

  let track = new Track(ID_A, TITLE_A, ARTISTS_A, RELEASE_A, NUMBER_A, DISC_NUMBER_A);
  let complementingTrack = new Track(ID_B, TITLE_B, ARTISTS_B, RELEASE_B, NUMBER_B, DISC_NUMBER_B);

  expect(track.getId()).toBe(ID_A);
  expect(track.getTitle()).toBe(TITLE_A);
  expect(track.getArtists()).toEqual(expect.arrayContaining(ARTISTS_A));
  expect(track.getArtists()).not.toEqual(expect.arrayContaining(ARTISTS_B));
  expect(track.getRelease()).toBe(RELEASE_A);
  expect(track.getNumber()).toBe(NUMBER_A);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER_A);
  track = track.complement(complementingTrack);
  expect(track.getId()).toBe(ID_A);
  expect(track.getTitle()).toBe(TITLE_A);
  expect(track.getArtists()).toEqual(expect.arrayContaining(ARTISTS_A));
  expect(track.getArtists()).toEqual(expect.arrayContaining(ARTISTS_B));
  expect(track.getRelease()).toBe(RELEASE_A);
  expect(track.getNumber()).toBe(NUMBER_A);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER_A);
});
