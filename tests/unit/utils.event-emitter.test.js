const EventEmitter = require("../../src/utils/event-emitter");

let eventEmitter;

beforeEach(() => {
  eventEmitter = new EventEmitter();
});

afterEach(() => {
  eventEmitter = null;
});

test("dispatch calls all registered listeners on namespace", () => {
  const CALLBACK1 = jest.fn();
  const CALLBACK2 = jest.fn();

  eventEmitter.on("NAMESPACE", CALLBACK1);
  eventEmitter.on("NAMESPACE", CALLBACK2);
  eventEmitter.dispatch("NAMESPACE");

  expect(CALLBACK1.mock.calls.length).toBe(1);
  expect(CALLBACK2.mock.calls.length).toBe(1);
});

test("dispatch doesnt call registered listeners on other namespaces", () => {
  const CALLBACK1 = jest.fn();
  const CALLBACK2 = jest.fn();

  eventEmitter.on("NAMESPACE1", CALLBACK1);
  eventEmitter.on("NAMESPACE2", CALLBACK2);
  eventEmitter.dispatch("NAMESPACE1");

  expect(CALLBACK1.mock.calls.length).toBe(1);
  expect(CALLBACK2.mock.calls.length).toBe(0);
});

test("unregistered listener doesnt get called anymore", () => {
  const CALLBACK1 = jest.fn();
  const CALLBACK2 = jest.fn();

  eventEmitter.on("NAMESPACE", CALLBACK1);
  eventEmitter.on("NAMESPACE", CALLBACK2);
  eventEmitter.dispatch("NAMESPACE");

  expect(CALLBACK1.mock.calls.length).toBe(1);
  expect(CALLBACK2.mock.calls.length).toBe(1);

  eventEmitter.off(CALLBACK1);
  eventEmitter.dispatch("NAMESPACE");

  expect(CALLBACK1.mock.calls.length).toBe(1);
  expect(CALLBACK2.mock.calls.length).toBe(2);
});

test("clearing all listener unregisters all listeners", () => {
  const CALLBACK1 = jest.fn();
  const CALLBACK2 = jest.fn();

  eventEmitter.on("NAMESPACE", CALLBACK1);
  eventEmitter.on("NAMESPACE", CALLBACK2);
  eventEmitter.dispatch("NAMESPACE");

  expect(CALLBACK1.mock.calls.length).toBe(1);
  expect(CALLBACK2.mock.calls.length).toBe(1);

  eventEmitter.clearListeners();
  eventEmitter.dispatch("NAMESPACE");

  expect(CALLBACK1.mock.calls.length).toBe(1);
  expect(CALLBACK2.mock.calls.length).toBe(1);
});

test("invoking listeners provides payload", () => {
  const CALLBACK1 = jest.fn();
  const CALLBACK2 = jest.fn();

  eventEmitter.on("NAMESPACE", CALLBACK1);
  eventEmitter.on("NAMESPACE", CALLBACK2);
  eventEmitter.dispatch("NAMESPACE", "data");

  expect(CALLBACK1.mock.calls[0]).toEqual(["data"]);
  expect(CALLBACK2.mock.calls[0]).toEqual(["data"]);
});
