const JobQueue = require("../../src/utils/job-queue");
const Logger = require("../../src/utils/logger");

const { OFF } = Logger;

Logger.setLogLevel(OFF);

test("subsequent jobs of same group get called simultaneously", () => {
  const jobQueue = new JobQueue({ limits: { default: 3 } });
  const resolves = [];

  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));

  expect(resolves.length).toBe(3);
  resolves.forEach((resolve) => resolve());
});

it("subsequent jobs of same group do not exceed group limit for simultaneous calling", async () => {
  const jobQueue = new JobQueue({ limits: { default: 2 } });
  const resolves = [];

  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));

  expect(resolves.length).toBe(2);
  resolves.forEach((resolve) => resolve());
  resolves.length = 0;
  await new Promise((resolve) => setTimeout(resolve, 100));
  expect(resolves.length).toBe(1);
  resolves.forEach((resolve) => resolve());
});

it("jobs of different groups get called sequentially", async () => {
  const jobQueue = new JobQueue({ limits: { default: 5, a: 5 } });
  const resolves = [];

  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)), {
    group: "a",
  });
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)), {
    group: "a",
  });
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)), {
    group: "a",
  });
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));

  expect(resolves.length).toBe(2);
  resolves.forEach((resolve) => resolve());
  resolves.length = 0;
  await new Promise((resolve) => setTimeout(resolve, 100));
  expect(resolves.length).toBe(1);
  resolves.forEach((resolve) => resolve());
  resolves.length = 0;
  await new Promise((resolve) => setTimeout(resolve, 100));
  expect(resolves.length).toBe(1);
  resolves.forEach((resolve) => resolve());
  resolves.length = 0;
  await new Promise((resolve) => setTimeout(resolve, 100));
  expect(resolves.length).toBe(2);
  resolves.forEach((resolve) => resolve());
  resolves.length = 0;
  await new Promise((resolve) => setTimeout(resolve, 100));
  expect(resolves.length).toBe(1);
  resolves.forEach((resolve) => resolve());
});

it("jobs with higher priority get called first", async () => {
  const jobQueue = new JobQueue({ limits: { default: 2, a: 2 } });
  const resolves = [];

  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)), {
    group: "placeholder",
  });

  const jobs = [
    { n: "A", g: "default", p: 50 },
    { n: "B", g: "default", p: 50 },
    { n: "C", g: "a", p: 50 },
    { n: "D", g: "a", p: 70 },
    { n: "E", g: "a", p: 70 },
  ];

  jobs.forEach((job) =>
    jobQueue.queueJob(
      () =>
        new Promise((resolve) =>
          resolves.push(() => {
            resolve();
            return job.n;
          })
        ),
      { group: job.g, priority: job.p }
    )
  );

  resolves.map((resolve) => resolve());
  resolves.length = 0;
  await new Promise((resolve) => setTimeout(resolve, 100));

  let results;
  expect(resolves.length).toBe(2);
  results = resolves.map((resolve) => resolve());
  resolves.length = 0;
  expect(results).toEqual(expect.arrayContaining(["D", "E"]));
  await new Promise((resolve) => setTimeout(resolve, 100));

  expect(resolves.length).toBe(2);
  results = resolves.map((resolve) => resolve());
  resolves.length = 0;
  expect(results).toEqual(expect.arrayContaining(["A", "B"]));
  await new Promise((resolve) => setTimeout(resolve, 100));

  expect(resolves.length).toBe(1);
  results = resolves.map((resolve) => resolve());
  resolves.length = 0;
  expect(results).toEqual(expect.arrayContaining(["C"]));
});

it("priority prepended jobs of the same group get called simultaneously", async () => {
  const jobQueue = new JobQueue({ limits: { default: 2, a: 2 } });
  const resolves = [];

  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)), {
    group: "placeholder",
  });

  const jobs = [
    { n: "A", g: "default", p: 60 },
    { n: "B", g: "a", p: 70 },
    { n: "C", g: "default", p: 60 },
    { n: "D", g: "a", p: 70 },
  ];

  jobs.forEach((job) =>
    jobQueue.queueJob(
      () =>
        new Promise((resolve) =>
          resolves.push(() => {
            resolve();
            return job.n;
          })
        ),
      { group: job.g, priority: job.p }
    )
  );

  resolves.map((resolve) => resolve());
  resolves.length = 0;
  await new Promise((resolve) => setTimeout(resolve, 100));

  let results;
  expect(resolves.length).toBe(2);
  results = resolves.map((resolve) => resolve());
  resolves.length = 0;
  expect(results).toEqual(expect.arrayContaining(["B", "D"]));
  await new Promise((resolve) => setTimeout(resolve, 100));

  expect(resolves.length).toBe(2);
  results = resolves.map((resolve) => resolve());
  resolves.length = 0;
  expect(results).toEqual(expect.arrayContaining(["A", "C"]));
});

test("retrieving the total amount of queued jobs returns the correct number", () => {
  const jobQueue = new JobQueue({ limits: { default: 1 } });

  jobQueue.queueJob(() => new Promise(() => {}));
  jobQueue.queueJob(() => new Promise(() => {}));
  jobQueue.queueJob(() => new Promise(() => {}));
  jobQueue.queueJob(() => new Promise(() => {}));

  expect(jobQueue.totalJobs()).toBe(3);
});

it("onQueueEmpty gets called after all jobs are done", async () => {
  const jobQueue = new JobQueue({ limits: { default: 3 } });
  const resolves = [];
  const callback = jest.fn();

  jobQueue.onQueueEmpty(callback);
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));
  jobQueue.queueJob(() => new Promise((resolve) => resolves.push(resolve)));

  resolves.forEach((resolve) => resolve());
  await new Promise((resolve) => setTimeout(resolve, 100));
  expect(callback.mock.calls.length).toBe(1);
});
