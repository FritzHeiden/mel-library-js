const DirectoryScanner = require("../../src/files/directory-scanner");
const { MP3, FLAC } = require("../../src/files/file");
const Logger = require("../../src/utils/logger");
const { OFF } = Logger;

const DIR_PATH = "/test/path/sub";

Logger.setLogLevel(OFF);

jest.mock(
  "FileSystem",
  () => ({
    readDir: (path) => {
      if (path === "/test/path/sub") {
        return Promise.resolve(["fileB.mp3", "dir"]);
      } else if (path === "/test/path/sub/dir") {
        return Promise.resolve(["fileC.mp3"]);
      } else if (path === "/test/path/extensions") {
        return Promise.resolve(["fileD.mp3", "fileE.flac"]);
      }
      return Promise.resolve(["fileA.mp3"]);
    },
    stats: (path) =>
      Promise.resolve({ isDirectory: path.split("/").pop() === "dir" }),
  }),
  { virtual: true }
);
const fileSystem = require("FileSystem");

describe("without configuration", () => {
  let directoryScanner;

  beforeEach(() => {
    directoryScanner = new DirectoryScanner();
  });

  afterEach(() => {
    directoryScanner = null;
  });

  it("scanning single directory throws errors", async () => {
    const DIRECTORY = "/test/path";
    const CALLBACK = () => {};

    await expect(
      directoryScanner.scanDir(DIRECTORY, CALLBACK)
    ).rejects.toThrow();
  });

  it("scanning multiple directories throws errors", async () => {
    const DIRECTORY = "/test/path";
    const CALLBACK = () => {};

    await expect(
      directoryScanner.scanDirs(DIRECTORY, CALLBACK)
    ).rejects.toThrow();
  });
});

describe("with configuration", () => {
  let directoryScanner;

  beforeEach(() => {
    directoryScanner = new DirectoryScanner();
    directoryScanner.setConfig({
      fileSystem,
      extensions: ["mp3", "flac"],
    });
  });

  afterEach(() => {
    directoryScanner = null;
  });

  it("scanning single directory returns files", async () => {
    const CALLBACK = jest.fn();

    await directoryScanner.scanDir(DIR_PATH, CALLBACK);

    expect(CALLBACK.mock.calls[0][0].getPath()).toBe(
      "/test/path/sub/fileB.mp3"
    );
    expect(CALLBACK.mock.calls[1][0].getPath()).toBe(
      "/test/path/sub/dir/fileC.mp3"
    );
  });

  it("scanning mutliple directories returns files", async () => {
    const CALLBACK = jest.fn();

    await directoryScanner.scanDirs(["/test/path", DIR_PATH], CALLBACK);

    expect(CALLBACK.mock.calls[0][0].getPath()).toBe("/test/path/fileA.mp3");
    expect(CALLBACK.mock.calls[1][0].getPath()).toBe(
      "/test/path/sub/fileB.mp3"
    );
    expect(CALLBACK.mock.calls[2][0].getPath()).toBe(
      "/test/path/sub/dir/fileC.mp3"
    );
  });

  it("scanned files have correct file types", async () => {
    const CALLBACK = jest.fn();

    await directoryScanner.scanDir("/test/path/extensions", CALLBACK);

    expect(CALLBACK.mock.calls[0][0].getType()).toBe(MP3);
    expect(CALLBACK.mock.calls[1][0].getType()).toBe(FLAC);
  });
});
