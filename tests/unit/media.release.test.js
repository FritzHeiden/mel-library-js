const Release = require("../../src/media/release");
const Artist = require("../../src/media/artist");
const Track = require("../../src/media/track");

test("data passed to constructor is retrievable by getters", () => {
  const ID = "A";
  const ARTIST = new Artist("A");
  const TITLE = "release";
  const YEAR = 2020;
  const TRACKS = [new Track("A")];
  const FEATURE_ARTISTS = [new Artist("B")];
  const RELEASE_COVER = {};

  let release = new Release(ID, ARTIST, TITLE, YEAR, TRACKS, FEATURE_ARTISTS, RELEASE_COVER);

  expect(release).toHaveProperty("getId");
  expect(release.getId).toBeInstanceOf(Function);
  expect(release.getId()).toBe(ID);

  expect(release).toHaveProperty("getArtist");
  expect(release.getArtist).toBeInstanceOf(Function);
  expect(release.getArtist()).toBe(ARTIST);

  expect(release).toHaveProperty("getTitle");
  expect(release.getTitle).toBeInstanceOf(Function);
  expect(release.getTitle()).toBe(TITLE);

  expect(release).toHaveProperty("getYear");
  expect(release.getYear).toBeInstanceOf(Function);
  expect(release.getYear()).toBe(YEAR);

  expect(release).toHaveProperty("getTracks");
  expect(release.getTracks).toBeInstanceOf(Function);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS));

  expect(release).toHaveProperty("getFeatureArtists");
  expect(release.getFeatureArtists).toBeInstanceOf(Function);
  expect(release.getFeatureArtists()).toEqual(expect.arrayContaining(FEATURE_ARTISTS));

  expect(release).toHaveProperty("getReleaseCover");
  expect(release.getReleaseCover).toBeInstanceOf(Function);
  expect(release.getReleaseCover()).toBe(RELEASE_COVER);
});

test("using setters changes values returned by getters accordingly", () => {
  const ID = "A";
  const ARTIST = new Artist("A");
  const TITLE = "release";
  const YEAR = 2020;
  const TRACKS = [new Track("A")];
  const FEATURE_ARTISTS = [new Artist("B")];
  const RELEASE_COVER = {};

  let release = new Release();

  expect(release.getId()).not.toBe(ID);
  release.setId(ID);
  expect(release.getId()).toBe(ID);

  expect(release.getArtist()).not.toBe(ARTIST);
  release.setArtist(ARTIST);
  expect(release.getArtist()).toBe(ARTIST);

  expect(release.getTitle()).not.toBe(TITLE);
  release.setTitle(TITLE);
  expect(release.getTitle()).toBe(TITLE);

  expect(release.getYear()).not.toBe(YEAR);
  release.setYear(YEAR);
  expect(release.getYear()).toBe(YEAR);

  expect(release.getTracks()).not.toEqual(expect.arrayContaining(TRACKS));
  release.setTracks(TRACKS);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS));

  expect(release.getFeatureArtists()).not.toEqual(expect.arrayContaining(FEATURE_ARTISTS));
  release.setFeatureArtists(FEATURE_ARTISTS);
  expect(release.getFeatureArtists()).toEqual(expect.arrayContaining(FEATURE_ARTISTS));

  expect(release.getReleaseCover()).not.toBe(RELEASE_COVER);
  release.setReleaseCover(RELEASE_COVER);
  expect(release.getReleaseCover()).toBe(RELEASE_COVER);
});

test("toString() returns data as string", () => {
  const ID = "A";
  const ARTIST = new Artist("A");
  const TITLE = "release";
  const YEAR = 2020;
  const TRACKS = [new Track("A")];
  const FEATURE_ARTISTS = [new Artist("B")];
  const RELEASE_COVER = {};

  let release = new Release(ID, ARTIST, TITLE, YEAR, TRACKS, FEATURE_ARTISTS, RELEASE_COVER);
  expect(release.toString()).toBe("Release {title: release, year: 2020, trackCount: 1, artist: undefined, featureArtists: , id: A}");
});

test("addTrack adds a track to the release and sets the tracks release accordingly", () => {
  const TRACK = new Track("A");

  let release = new Release();

  expect(release.getTracks()).not.toContain(TRACK);
  expect(TRACK.getRelease()).not.toBe(release);
  release.addTrack(TRACK);
  expect(release.getTracks()).toContain(TRACK);
  expect(TRACK.getRelease()).toBe(release);
});

test("addTracks adds multiple track to the release and sets the track release accordingly", () => {
  const TRACK_A = new Track("A");
  const TRACK_B = new Track("B");

  let release = new Release();

  expect(release.getTracks()).not.toContain(TRACK_A);
  expect(release.getTracks()).not.toContain(TRACK_B);
  expect(TRACK_A.getRelease()).not.toBe(release);
  expect(TRACK_B.getRelease()).not.toBe(release);
  release.addTracks([TRACK_A, TRACK_B]);
  expect(release.getTracks()).toContain(TRACK_A);
  expect(release.getTracks()).toContain(TRACK_B);
  expect(TRACK_A.getRelease()).toBe(release);
  expect(TRACK_B.getRelease()).toBe(release);
});

test("addFeatureArtist adds a feature artist to the track", () => {
  const ARTIST = new Artist("A");

  let release = new Release();

  expect(release.getFeatureArtists()).not.toContain(ARTIST);
  release.addFeatureArtist(ARTIST);
  expect(release.getFeatureArtists()).toContain(ARTIST);
});

test("addFeatureArtists adds multiple feature artists to the track", () => {
  const ARTIST_A = new Artist("A");
  const ARTIST_B = new Artist("B");

  let release = new Release();

  expect(release.getFeatureArtists()).not.toContain(ARTIST_A);
  expect(release.getFeatureArtists()).not.toContain(ARTIST_B);
  release.addFeatureArtists([ARTIST_A, ARTIST_B]);
  expect(release.getFeatureArtists()).toContain(ARTIST_A);
  expect(release.getFeatureArtists()).toContain(ARTIST_B);
});

test("deleteTrack deletes a track from the release using its id", () => {
  const TRACK = new Track("A");

  let release = new Release();
  release.addTrack(TRACK);

  expect(release.getTracks()).toContain(TRACK);
  release.deleteTrack("A");
  expect(release.getTracks()).not.toContain(TRACK);
});

test("complement adds missing data from provided release", () => {
  const ID = "A";
  const ARTIST = new Artist("A");
  const TITLE = "release";
  const YEAR = 2020;
  const TRACKS = [new Track("A")];
  const FEATURE_ARTISTS = [new Artist("B")];
  const RELEASE_COVER = {};

  let release = new Release();
  let complementingRelease = new Release(ID, ARTIST, TITLE, YEAR, TRACKS, FEATURE_ARTISTS, RELEASE_COVER);

  expect(release.getId()).not.toBe(ID);
  expect(release.getArtist()).not.toBe(ARTIST);
  expect(release.getTitle()).not.toBe(TITLE);
  expect(release.getYear()).not.toBe(YEAR);
  expect(release.getTracks()).not.toEqual(expect.arrayContaining(TRACKS));
  expect(release.getFeatureArtists()).not.toEqual(expect.arrayContaining(FEATURE_ARTISTS));
  expect(release.getReleaseCover()).not.toBe(RELEASE_COVER);
  release = release.complement(complementingRelease);
  expect(release.getId()).toBe(ID);
  expect(release.getArtist()).toBe(ARTIST);
  expect(release.getTitle()).toBe(TITLE);
  expect(release.getYear()).toBe(YEAR);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS));
  expect(release.getFeatureArtists()).toEqual(expect.arrayContaining(FEATURE_ARTISTS));
  expect(release.getReleaseCover()).toBe(RELEASE_COVER);
});

test("complement doesnt overwrite existing data", () => {
  const ID_A = "A";
  const ID_B = "B";
  const ARTIST_A = new Artist("A");
  const ARTIST_B = new Artist("B");
  const TITLE_A = "releaseA";
  const TITLE_B = "releaseB";
  const YEAR_A = 2020;
  const YEAR_B = 2019;
  const TRACKS_A = [new Track("A")];
  const TRACKS_B = [new Track("B")];
  const FEATURE_ARTISTS_A = [new Artist("C")];
  const FEATURE_ARTISTS_B = [new Artist("D")];
  const RELEASE_COVER_A = {};
  const RELEASE_COVER_B = {};

  let release = new Release(ID_A, ARTIST_A, TITLE_A, YEAR_A, TRACKS_A, FEATURE_ARTISTS_A, RELEASE_COVER_A);
  let complementingRelease = new Release(ID_B, ARTIST_B, TITLE_B, YEAR_B, TRACKS_B, FEATURE_ARTISTS_B, RELEASE_COVER_B);

  expect(release.getId()).toBe(ID_A);
  expect(release.getArtist()).toBe(ARTIST_A);
  expect(release.getTitle()).toBe(TITLE_A);
  expect(release.getYear()).toBe(YEAR_A);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS_A));
  expect(release.getFeatureArtists()).toEqual(expect.arrayContaining(FEATURE_ARTISTS_A));
  expect(release.getReleaseCover()).toBe(RELEASE_COVER_A);
  release = release.complement(complementingRelease);
  expect(release.getId()).toBe(ID_A);
  expect(release.getArtist()).toBe(ARTIST_A);
  expect(release.getTitle()).toBe(TITLE_A);
  expect(release.getYear()).toBe(YEAR_A);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS_A));
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS_B));
  expect(release.getFeatureArtists()).toEqual(expect.arrayContaining(FEATURE_ARTISTS_A));
  expect(release.getFeatureArtists()).toEqual(expect.arrayContaining(FEATURE_ARTISTS_B));
  expect(release.getReleaseCover()).toBe(RELEASE_COVER_A);
});

test("deleteReleaseCoverBuffer deletes the buffer of the album cover from the release", () => {
  const RELEASE_COVER = new ArrayBuffer();

  let release = new Release();
  release.setReleaseCover(RELEASE_COVER);
  
  expect(release.getReleaseCover()).toBe(RELEASE_COVER);
  release.deleteReleaseCoverBuffer();
  expect(release.getReleaseCover()).toBe(null);
});
